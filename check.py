from OpenSSL import SSL
from cryptography import x509
from cryptography.x509.oid import NameOID
import idna
from datetime import datetime
from dateutil.relativedelta import relativedelta
import colorama

from socket import socket
from collections import namedtuple

today = datetime.now()
one_mouths_ago = today + relativedelta(months=1)
soon_expirate = []
expirate = []

HostInfo = namedtuple(field_names='cert hostname peername', typename='HostInfo')


def get_certificate(hostname, port):
    hostname_idna = idna.encode(hostname)
    sock = socket()

    sock.connect((hostname, port))
    peername = sock.getpeername()
    ctx = SSL.Context(SSL.SSLv23_METHOD)
    ctx.check_hostname = False
    ctx.verify_mode = SSL.VERIFY_NONE

    sock_ssl = SSL.Connection(ctx, sock)
    sock_ssl.set_connect_state()
    sock_ssl.set_tlsext_host_name(hostname_idna)
    sock_ssl.do_handshake()
    cert = sock_ssl.get_peer_certificate()
    crypto_cert = cert.to_cryptography()
    sock_ssl.close()
    sock.close()

    return HostInfo(cert=crypto_cert, peername=peername, hostname=hostname)


def print_info(hostinfo):
    expiration = hostinfo.cert.not_valid_after
    if not expiration > one_mouths_ago and expiration > today:
        soon_expirate.append(l.strip()+ " : " + expiration.strftime("%d/%m/%Y"))
    elif expiration < today:
        expirate.append(l.strip()+ " : " + expiration.strftime("%d/%m/%Y"))
    print(l.strip()+ " : " + hostinfo.cert.not_valid_after.strftime("%d/%m/%Y"))
    

with open("domains.txt", "r") as file:
    for l in file:
        kek = get_certificate(l.strip(), 443)
        print_info(kek)


print("\n----- DOMAINE QUI EXPIRE DANS MOINS DE 30 JOURS :")
if len(soon_expirate) == 0:
    print(colorama.Fore.GREEN + "OK\n")
    print(colorama.Style.RESET_ALL)
for d in soon_expirate:
    print(colorama.Fore.YELLOW + d)
    print(colorama.Style.RESET_ALL)

print("----- DOMAINE EXPIRE :")
if len(expirate) == 0:
    print(colorama.Fore.GREEN + "OK\n")
    print(colorama.Style.RESET_ALL)
for d in expirate:
    print(colorama.Fore.RED + d)
